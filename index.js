const express = require('express');
const bodyParser = require('body-parser');
//const http = require('http');
const cors = require("cors");
const router = express.Router();
let app = express();
app.use('/static', express.static('public'))
const port = process.env.PORT || 3000;
//const hostname = 'http://ec2-13-126-53-6.ap-south-1.compute.amazonaws.com/';
let userController = require('./controllers/userController');


app.use(bodyParser.json({ limit: '20mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));
const {initPayment, responsePayment} = require("./paytm/services/index");
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// const server = http.createServer((req, res) => {
//   res.statusCode = 200;
//   res.setHeader('Content-Type', 'text/plain');
//   res.end('Hello World\n');
// });
//***************url start***********///
app.use('/', userController);

var validator =  function (req, res, next) {
    next();
}
// app.use(validator); use for globally 

app.get("/", (req, res) => {
    
    res.sendFile(__dirname + '/public/index.html');
});

app.get("/user", validator, (req, res) => {
    res.send("User data access");
});

app.get("/paywithpaytm/:amount", (req, res) => {
    initPayment(req.params.amount).then(
        success => {
            // res.render("paytmRedirect.ejs", {
            //     resultData: success,
            //     paytmFinalUrl: process.env.PAYTM_FINAL_URL
            // });
          return  res.status(200).json({
                'status': true,
                'message': "success",
                'result': success ? success : {}
            });
        },
        error => {
            res.send(error);
        }
    );
});

app.post("/paywithpaytmresponse", (req, res) => {
    responsePayment(req.body).then(
        success => {
            return res.redirect('http://localhost:4200/buybook');
        },
        error => {
            res.send(error);
        }
    );
});

module.exports = router;
app.listen(port);
// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });