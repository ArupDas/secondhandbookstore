require('../models/db')
const express = require('express');
const bodyParser = require('body-parser');
var nodemailer = require('nodemailer');
var paginate = require('jw-paginate');
const router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const PostBookDetails = mongoose.model('PostBookDetails');
const BuyBookDetails = mongoose.model('BuyBookDetails');
var aws = require('aws-sdk');


aws.config.update({
    accessKeyId: 'AKIASGML3KVAD535CDUI',
    secretAccessKey: '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',
    region: 'ap-south-1'
});
var ses = new aws.SES({ apiVersion: '2012-10-17' });


router.post('/api/userRegistration', (req, res) => {
    let user = new User();
    user.username = req.body.username;
    user.email = req.body.email;
    user.password = req.body.password;
    user.mobilenumber = req.body.mbnumber;
    user.nickname = req.body.nickname;
    User.findOne({ email: { $regex: user.email, $options: "$i" } }, (err, doc) => {
        if (err) {
            console.log(err);
        } else if (doc === null) {
            user.save((err) => {
                if (err) {
                    console.log(err);
                }
                else {
                   // res.json("data inserted");
                    sendResponse(res, 200, true, "Registration successfull",'');
                }
            });

        } else if (doc !== null) {
                sendResponse(res, 200, false, "Email already exist, Please login",'');
        }
    });
});

router.post('/api/userLogin', (req, res) => {
    let user = new User();
    user.password = req.body.password;
    user.email = req.body.email;
    User.findOne({ email: { $regex: user.email, $options: "$i" } }, (err, doc) => {
        if (err) {
            console.log(err);
        } else if (doc === null) {
            sendResponse(res, 200, false, "Email does not exit", "");

        } else if (doc !== null) {

            if (user.password === doc.password) {
                sendResponse(res, 200, true, "login successfull", doc);

            } else {
                sendResponse(res, 200, false, "password mismatch", "");
            }

        }
    });
});

router.post('/api/forgetPassword',(req,res)=>{
    let user = new User();
    user.email = req.body.email;
    User.findOne({ email: { $regex: user.email, $options: "$i" } }, (err, doc) => {
        if (err) {
            console.log(err);
        } else if (doc === null) {
            
            sendResponse(res, 200, false, "Email does not exit,please SignUp", "");

        } else if (doc !== null) {
            ses.sendEmail({
                Source: 'secondhandbookstore.info@gmail.com',
                Destination: {
                    ToAddresses: [req.body.email]
                },
                Message: {
                    Subject: {
                        Data: 'Your Password'
                    },
                    Body: {
                        Html: {
                            Data: `Your password :- ${doc.password}`
                        }
                    }
                }
            }
                , function (err, data) {
                    if (err) {
                        console.log(err);
                    } else {

                       // res.json({ data });
                        sendResponse(res, 200, true, "send successfull", data);
                    }
                });

        }
    });
})

router.post('/api/postBookDetail/:_userid', (req, res) => {
    let postBookDetails = new PostBookDetails();
    postBookDetails.category = req.body.category;
    postBookDetails.department = req.body.department;
    postBookDetails.bookname = req.body.bookname;
    postBookDetails.authorname = req.body.authorname;
    postBookDetails.isdnnumber = req.body.isdnnumber;
    postBookDetails.frontbookimage = req.body.frontbookimage;
    postBookDetails.edition = req.body.edition;
    postBookDetails.actualprice = req.body.actualprice;
    postBookDetails.sellingprice = req.body.sellingprice;
    postBookDetails.description = req.body.description;
    postBookDetails.name = req.body.name;
    postBookDetails.email = req.body.email;
    postBookDetails.mbnumber = req.body.mbnumber;
    postBookDetails.state = req.body.state;
    postBookDetails.city = req.body.city;
    postBookDetails.pickaddress = req.body.pickaddress;
    postBookDetails.pincode = req.body.pincode;
    postBookDetails.upi = req.body.upi;
    postBookDetails.Date = new Date().toLocaleDateString('en-GB', {
        day: 'numeric', month: 'short', year: 'numeric'
      }).replace(/ /g, '-');
    postBookDetails._userid = req.params._userid;
    postBookDetails.bookstatus = '0';   //0 means book not sold, 1 means sold
    postBookDetails.save((err) => {
        if (err) {
            console.log(err);
        }
        else {
            res.json("data inserted");
        }
    });
});

router.get('/api/getBookDetails/:pagenumber', (req, res) => {
    let pagenumber = req.params.pagenumber;
    PostBookDetails.find({ bookstatus: '0' }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            const item = doc;
            const page = pagenumber;
            const pageSize = 20;
            const pager = paginate(item.length, page, pageSize);
            const pageOfItems = item.slice(pager.startIndex, pager.endIndex + 1);
            return res.json({ pager, pageOfItems })
        }
    }).sort({ "_id": -1 });
});

router.get('/api/getCategoryDetails/:category/:pagenumber', (req, res) => {
    let pagenumber = req.params.pagenumber;
    let category = req.params.category;
    PostBookDetails.find({ category: category, bookstatus: '0' }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            const item = doc;
            const page = pagenumber;
            const pageSize = 20;
            const pager = paginate(item.length, page, pageSize);
            const pageOfItems = item.slice(pager.startIndex, pager.endIndex + 1);
            return res.json({ pager, pageOfItems })
        }
    })
});
router.get('/api/getBranchDetails/:branch/:pagenumber', (req, res) => {
    let pagenumber = req.params.pagenumber;
    let branch = req.params.branch;
    PostBookDetails.find({ department: branch, bookstatus: '0' }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            const item = doc;
            const page = pagenumber;
            const pageSize = 20;
            const pager = paginate(item.length, page, pageSize);
            const pageOfItems = item.slice(pager.startIndex, pager.endIndex + 1);
            return res.json({ pager, pageOfItems })
        }
    })
});

router.get('/api/searchitemWithAll/:searchData/:pagenumber1/:category/:branch', (req, res) => {
    let userid = req.params._userid;
    let searchData = req.params.searchData;
    let pagenumber1 = req.params.pagenumber1;
    let category = req.params.category;
    let branch = req.params.branch;
    Adduser.find({
        $and: [{
            $or: [{ bookname: { $regex: searchData, $options: "$i" } },
            { authorname: { $regex: searchData, $options: "$i" } }
            ]
        }, { category: category }, { department: branch }, { bookstatus: '0' }]
    }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            const item = doc;
            const page = pagenumber1;
            const pageSize = 20;
            const pager = paginate(item.length, page, pageSize);
            const pageOfItems = item.slice(pager.startIndex, pager.endIndex + 1);
            return res.json({ pager, pageOfItems })
        }
    })
});

router.get('/api/searchitem/:searchData/:pagenumber1', (req, res) => {
    let searchData = req.params.searchData;
    let pagenumber1 = req.params.pagenumber1;
    PostBookDetails.find({
        $and: [{
            $or: [{ bookname: { $regex: searchData, $options: "$i" } },
            { authorname: { $regex: searchData, $options: "$i" } }]
        }, { bookstatus: '0' }]
    }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            const item = doc;
            const page = pagenumber1;
            const pageSize = 20;
            const pager = paginate(item.length, page, pageSize);
            const pageOfItems = item.slice(pager.startIndex, pager.endIndex + 1);
            return res.json({ pager, pageOfItems })
        }
    })
});

router.get('/api/sellbookdetailsearchitem/:searchData/:pagenumber1/:userid', (req, res) => {
    let searchData = req.params.searchData;
    let pagenumber1 = req.params.pagenumber1;
    let userid = req.params.userid;
    PostBookDetails.find({
        $and: [{
            $or: [{ bookname: { $regex: searchData, $options: "$i" } },
            { authorname: { $regex: searchData, $options: "$i" } },
            { isdnnumber: { $regex: searchData, $options: "$i" } }]
        }, { _userid: userid }]
    }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            const item = doc;
            const page = pagenumber1;
            const pageSize = 20;
            const pager = paginate(item.length, page, pageSize);
            const pageOfItems = item.slice(pager.startIndex, pager.endIndex + 1);
            return res.json({ pager, pageOfItems })
        }
    })
});

router.get('/api/getBuyBookDetail/:postbookid', (req, res) => {
    let postbookid = req.params.postbookid;
    PostBookDetails.findOne({ _id: postbookid }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            res.json({ doc });
        }
    })
});

router.get('/api/userAccountDetails/:userid', (req, res) => {
    let userid = req.params.userid;
    User.findOne({ _id: userid }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            res.json({ doc });
        }
    })

});

router.get('/api/buyBookDetails/:userid', (req, res) => {
    let userid = req.params.userid;
    BuyBookDetails.find({ _userid: userid }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            res.json({ doc });
        }
    }).sort({ "_id": -1 });
})
router.get('/api/sellBookDetails/:userid/:pagenumber', (req, res) => {
    let userid = req.params.userid;
    let pagenumber = req.params.pagenumber;
    PostBookDetails.find({ _userid: userid }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            const item = doc;
            const page = pagenumber;
            const pageSize = 20;
            const pager = paginate(item.length, page, pageSize);
            const pageOfItems = item.slice(pager.startIndex, pager.endIndex + 1);
            return res.json({ pager, pageOfItems })
        }
    }).sort({ "_id": -1 });
});

router.get('/api/deletebook/:postbookid', (req, res) => {
    let postbookid = req.params.postbookid;
    PostBookDetails.deleteOne({ _id: postbookid }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            res.json({ doc });
        }
    })
});

router.post('/api/editBookDetail',(req,res)=>{
    PostBookDetails.findOneAndUpdate({_id:req.body.postbookid},
        {$set:{category:req.body.category,
            department:req.body.department,
            bookname:req.body.bookname,
            authorname:req.body.authorname,
            isdnnumber:req.body.isdnnumber,
            edition:req.body.edition,
            actualprice:req.body.actualprice,
            sellingprice:req.body.sellingprice,
            description:req.body.description,
            name:req.body.name,
            email:req.body.email,
            mbnumber:req.body.mbnumber,
            state:req.body.state,
            city:req.body.city,
            pickaddress:req.body.pickaddress,
            pincode:req.body.pincode,
            upi:req.body.upi
        }},function(err,doc){
            if (err) {
                console.log(err);
            } else {
                res.json({ doc });
            }
        })
})

router.post('/api/sendBuyerDetail', (req, res) => {
    let companyMail = "secondhandbookstore.info@gmail.com";
    let buyBookDetails = new BuyBookDetails();
    let invoiceId = Date.now() + Math.random();
    let currentDate = new Date().toLocaleDateString('en-GB', {
        day: 'numeric', month: 'short', year: 'numeric'
      }).replace(/ /g, '-');
    
    buyBookDetails.category = req.body.category;
    buyBookDetails.department = req.body.department;
    buyBookDetails.bookname = req.body.bookname;
    buyBookDetails.authorname = req.body.authorname;
    buyBookDetails.isdnnumber = req.body.isdnnumber;
    buyBookDetails.edition = req.body.edition;
    buyBookDetails.actualprice = req.body.actualprice;
    buyBookDetails.sellingprice = req.body.sellingprice;
    buyBookDetails.name = req.body.name;
    buyBookDetails.email = req.body.email;
    buyBookDetails.mbnumber = req.body.mbnumber;
    buyBookDetails.postbookid = req.body.postbookid;
    buyBookDetails.deliveryaddress = req.body.deliveryaddress;
    buyBookDetails.Date = currentDate;
    buyBookDetails._userid = req.body._userid;
    buyBookDetails.gstTax = req.body.gstTax;
    buyBookDetails.paymentMode = req.body.paymentMode;
    buyBookDetails.deliveryCharge = req.body.deliveryCharge;
    buyBookDetails.totalPrice = req.body.totalPrice;
    buyBookDetails.invoiceId = invoiceId;
    buyBookDetails.currentDate = currentDate;
    buyBookDetails.save((err) => {
        if (err) {
            console.log(err);
        }
        else {
            let postbookid = req.body.postbookid;
            PostBookDetails.findOneAndUpdate({ _id: postbookid },
                { $set: { bookstatus: '1' } }, function (err, doc) {
                    if (err) {
                        console.log(err);
                    } else {
                        ses.sendEmail({
                            Source: 'secondhandbookstore.info@gmail.com',
                            Destination: {
                                ToAddresses: [req.body.email, companyMail]
                            },
                            Message: {
                                Subject: {
                                    Data: 'Your Confirmation'
                                },
                                Body: {
                                    Html: {
                                        Data: `<!doctype html>\n<html>\n<head>\n    <meta charset=\"utf-8\">\n    <style>\n    .invoice-box {\n        max-width: 800px;\n        margin: auto;\n        padding: 30px;\n        border: 1px solid #eee;\n        box-shadow: 0 0 10px rgba(0, 0, 0, .15);\n        font-size: 16px;\n        line-height: 24px;\n        font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif;\n        color: #555;\n    }\n    \n    .invoice-box table {\n        width: 100%;\n        line-height: inherit;\n        text-align: left;\n    }\n    \n    .invoice-box table td {\n        padding: 5px;\n        vertical-align: top;\n    }\n    \n    .invoice-box table tr td:nth-child(2) {\n        text-align: right;\n    }\n    \n    .invoice-box table tr.top table td {\n        padding-bottom: 20px;\n    }\n    \n    .invoice-box table tr.top table td.title {\n        font-size: 45px;\n        line-height: 45px;\n        color: #333;\n    }\n    \n    .invoice-box table tr.information table td {\n        padding-bottom: 40px;\n    }\n    \n    .invoice-box table tr.heading td {\n        background: #eee;\n        border-bottom: 1px solid #ddd;\n        font-weight: bold;\n    }\n    \n    .invoice-box table tr.details td {\n        padding-bottom: 20px;\n    }\n    \n    .invoice-box table tr.item td{\n        border-bottom: 1px solid #eee;\n    }\n    \n    .invoice-box table tr.item.last td {\n        border-bottom: none;\n    }\n    \n    .invoice-box table tr.total td:nth-child(2) {\n        border-top: 2px solid #eee;\n        font-weight: bold;\n    }\n    \n    @media only screen and (max-width: 600px) {\n        .invoice-box table tr.top table td {\n            width: 100%;\n            display: block;\n            text-align: center;\n        }\n        \n        .invoice-box table tr.information table td {\n            width: 100%;\n            display: block;\n            text-align: center;\n        }\n    }\n    \n    /** RTL **/\n    .rtl {\n        direction: rtl;\n        font-family: Tahoma, \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif;\n    }\n    \n    .rtl table {\n        text-align: right;\n    }\n    \n    .rtl table tr td:nth-child(2) {\n        text-align: left;\n    }\n    </style>\n</head>\n\n<body>\n    <div class=\"invoice-box\">\n        <table cellpadding=\"0\" cellspacing=\"0\">\n            <tr class=\"top\">\n                <td colspan=\"2\">\n                    <table>\n                        <tr>\n                            <td style=\"text-align:center\">\n                               secondhandbookstore.in\n                            </td>\n                        </tr>\n                    </table>\n                </td>\n            </tr>\n            \n            <tr class=\"information\">\n                <td colspan=\"2\">\n                    <table>\n                        <tr>\n                            <td>\n                               Invoice #: ${invoiceId}<br>\n                                Created: ${currentDate}<br>\n                                Buy Date: ${currentDate}\n                            </td>\n                            \n                            <td>\n                                ${req.body.name}<br>\n                                ${req.body.email}<br>\n                               Address:- ${req.body.deliveryaddress}<br>\n                               \n                            </td>\n                        </tr>\n                    </table>\n                </td>\n            </tr>\n            \n            <tr class=\"heading\">\n                <td>\n                    Payment\n                </td>\n                \n                <td>\n                    Price\n                </td>\n            </tr>\n            \n            <tr class=\"details\">\n                <td>\n                    Delivery Charges\n                </td>\n                \n                <td>\n                     ${req.body.deliveryCharge}\n                </td>\n            </tr>\n            \n            <tr class=\"details\">\n                <td>\n                    G.S.T\n                </td>\n                \n                <td>\n                    Rs. ${req.body.gstTax}\n                </td>\n            </tr>\n            \n            <tr class=\"details\">\n                <td>\n                    ${req.body.paymentMode}\n                </td>\n                \n                <td>\n                   Total =  Rs. ${req.body.totalPrice}\n                </td>\n            </tr>\n            \n            <tr class=\"heading\">\n                <td>\n                    Basic Information\n                </td>\n                \n                <td>\n                    Description\n                </td>\n            </tr>\n            \n            <tr class=\"item\">\n                <td>\n                    Book Name\n                </td>\n                \n                <td>\n                    ${req.body.bookname}\n                </td>\n            </tr>\n            \n            <tr class=\"item\">\n                <td>\n                    Author Name\n                </td>\n                \n                <td>\n                    ${req.body.authorname}\n                </td>\n            </tr>\n                <tr class=\"item\">\n                <td>\n                    Category\n                </td>\n                \n                <td>\n                    ${req.body.category}\n                </td>\n            </tr>\n                <tr class=\"item\">\n                <td>\n                    Department\n                </td>\n                \n                <td>\n                    ${req.body.department}\n                </td>\n            </tr>\n                <tr class=\"item\">\n                <td>\n                    Edition\n                </td>\n                \n                <td>\n                    ${req.body.edition}\n                </td>\n            </tr>\n                <tr class=\"item\">\n                <td>\n                   Buyer Mobile No\n                </td>\n                \n                <td>\n                    ${req.body.mbnumber}\n                </td>\n            </tr>\n            \n            <tr class=\"item last\">\n                <td>\n                    ISBN No.\n                </td>\n                \n                <td>\n                    ${req.body.isdnnumber}\n                </td>\n            </tr>\n        </table>\n    </div>\n</body>\n</html>`
                                    }
                                }
                            }
                        }
                            , function (err, data) {
                                if (err) {
                                    console.log(err);
                                } else {
        
                                    res.json({ data });
                                }
                            });
                    }
                });
        }
    });
});

sendResponse = (res, statusCode, status, message, result) => {
    res.status(statusCode).json({
        "status": status,
        "message": message,
        "result": result,
    });
};


module.exports = router;