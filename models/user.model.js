const mongoose = require('mongoose');
/****************registration Schema**************************/ 
var userSchema = new mongoose.Schema({
    username: {
        type: String
    },
    password: {
        type: String
    },
    email: {
        type: String
    },
    country: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    _userid: {
        type: String
    },
    mobilenumber: {
        type: String
    },
    nickname: {
        type: String
    }
});

mongoose.model('User', userSchema);


/****************post Book Schema**************************/ 
var postBookSchema = new mongoose.Schema({
    _userid: {
        type: String
    },
    category: {
        type: String
    },
    department: {
        type: String
    },
    bookname: {
        type: String
    },
    authorname: {
        type: String
    },
    isdnnumber: {
        type: String
    },
    frontbookimage: {
        type: String
    },
    backbookimage: {
        type: String
    },
    edition: {
        type: String
    },
    actualprice: {
        type: String
    },
    sellingprice: {
        type: String
    },
    description: {
        type: String
    },
    name: {
        type: String
    },
    email: {
        type: String
    },
    state: {
        type: String
    },
    city: {
        type: String
    },
    pickaddress: {
        type: String
    },
    mbnumber: {
        type: String
    },
    pincode: {
        type: String
    },
    upi: {
        type: String
    },
    bookstatus:{
        type:String
    },
    Date: {
        type:String
    }
});

mongoose.model('PostBookDetails', postBookSchema);

/*****************buy book schema**************/

/****************post Book Schema**************************/ 
var buyBookSchema = new mongoose.Schema({
    _userid: {
        type: String
    },
    category: {
        type: String
    },
    department: {
        type: String
    },
    bookname: {
        type: String
    },
    authorname: {
        type: String
    },
    isdnnumber: {
        type: String
    },
    edition: {
        type: String
    },
    actualprice: {
        type: String
    },
    sellingprice: {
        type: String
    },
    name: {
        type: String
    },
    email: {
        type: String
    },
    deliveryaddress: {
        type: String
    },
    mbnumber: {
        type: String
    },
    Date: {
        type: String
    },
    postbookid: {
        type: String
    },
    gstTax: {
        type: String
    },
    paymentMode:{
        type:String
    },
    deliveryCharge:{
        type:String
    },
    totalPrice:{
        type:String
    },
    invoiceId:{
        type:String
    },
    currentDate:{
        type:String
    }
    
});

mongoose.model('BuyBookDetails', buyBookSchema);